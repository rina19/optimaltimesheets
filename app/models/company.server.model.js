'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var TrimmedString = {type:String, trim:true};

/**
 * Company Schema
 */
var CompanySchema = new Schema({
	name: TrimmedString,
	address:{
		line1: TrimmedString,
		line2:TrimmedString,
		city: TrimmedString,
		zip: TrimmedString,
		state: TrimmedString,
		country: TrimmedString
	},
	email: {
		type: String,
		trim: true,
		match: [/.+\@.+\..+/, 'Please fill a valid email address'],
		lowercase: true
	},
	phone: TrimmedString,
	logo: {mime: String, data: String}
});

mongoose.model('Company', CompanySchema);